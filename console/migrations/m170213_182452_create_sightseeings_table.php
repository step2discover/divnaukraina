<?php

use yii\db\Migration;

/**
 * Handles the creation of table `sightseeings`.
 */
class m170213_182452_create_sightseeings_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
		$this->createTable('sightseeings', [
            'id' => $this->primaryKey(),
            'name'=> $this->string()->notNull(),
            'description'=>$this->text()->notNull(),
            'shortDescription'=>$this->text()->notNull(),
            'rlongtitude'=>$this->decimal(10,7)->notNull(),
            'rlatitude'=>$this->decimal(9,7)->notNull(),            
            'fY'=>$this->float(),
            'fX'=>$this->float(),              
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('sightseeings');
    }
}
